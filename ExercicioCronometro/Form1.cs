﻿namespace ExercicioCronometro
{
    using System;
    using System.Windows.Forms;
    public partial class Form1 : Form
    {
        private readonly Cronometro _cronometro;
        public Form1()
        {
            InitializeComponent();
            _cronometro = new ExercicioCronometro.Cronometro();
        }

        private void buttonOnOff_Click(object sender, EventArgs e)
        {
            if (_cronometro.ClockState())
            {
                _cronometro.StopClock();
                buttonOnOff.Text = "Ligar";
                TimerRelogio.Enabled = false;
                //labelContador.Text = _cronometro.GetTimeStamp().ToString();
            }
            else
            {
                _cronometro.StartClock();
                buttonOnOff.Text = "Desligar";
                TimerRelogio.Enabled = true;
            }
        }

        private void UpdateLabel()
        {
            var Tempo = DateTime.Now - _cronometro.StartTime();
            labelContador.Text = String.Format("{0:D2}:{1:D2}:{2:D2}:{3}", Tempo.Hours, Tempo.Minutes, Tempo.Seconds, Tempo.Milliseconds);
        }

        private void TimerRelogio_Tick(object sender, EventArgs e)
        {
            UpdateLabel();
        }
    }
}
