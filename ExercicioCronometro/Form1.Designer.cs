﻿namespace ExercicioCronometro
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.buttonOnOff = new System.Windows.Forms.Button();
            this.labelContador = new System.Windows.Forms.Label();
            this.TimerRelogio = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // buttonOnOff
            // 
            this.buttonOnOff.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOnOff.Location = new System.Drawing.Point(28, 63);
            this.buttonOnOff.Name = "buttonOnOff";
            this.buttonOnOff.Size = new System.Drawing.Size(99, 47);
            this.buttonOnOff.TabIndex = 0;
            this.buttonOnOff.Text = "Ligar";
            this.buttonOnOff.UseVisualStyleBackColor = true;
            this.buttonOnOff.Click += new System.EventHandler(this.buttonOnOff_Click);
            // 
            // labelContador
            // 
            this.labelContador.AutoSize = true;
            this.labelContador.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelContador.Location = new System.Drawing.Point(24, 23);
            this.labelContador.Name = "labelContador";
            this.labelContador.Size = new System.Drawing.Size(114, 20);
            this.labelContador.TabIndex = 1;
            this.labelContador.Text = "00:00:00:000";
            // 
            // TimerRelogio
            // 
            this.TimerRelogio.Tick += new System.EventHandler(this.TimerRelogio_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(148, 116);
            this.Controls.Add(this.labelContador);
            this.Controls.Add(this.buttonOnOff);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonOnOff;
        private System.Windows.Forms.Label labelContador;
        private System.Windows.Forms.Timer TimerRelogio;
    }
}

