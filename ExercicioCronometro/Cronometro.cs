﻿namespace ExercicioCronometro
{
    using System;
    public class Cronometro
    {
        #region atributos

        private DateTime _start;
        private DateTime _stop;
        private bool _isrunning;

        #endregion

        #region Metodos
        public void StartClock()
        {
            if (_isrunning)
                throw new InvalidOperationException("O cronometro ja esta ligado");
            _start = DateTime.Now;
            _isrunning = true;
        }

        public void StopClock()
        {
            if (!_isrunning)
                throw new InvalidOperationException("O cronometro ja esta desligado");
            _stop = DateTime.Now;
            _isrunning = false;
        }

        public TimeSpan GetTimeStamp()
        {
            return _stop - _start;
        }

        public bool ClockState()
        {
            return _isrunning;
        }
        public DateTime StartTime()
        {
            return _start;
        }
        #endregion
    }
}
